#!/usr/bin/bash

# Nicolas Abelanet - Jan 11th, 2023
# Computer Forensics

echo -e "Testing Argument Inequality/Equality:"

# Determine which argument is larger
if [ $1 -gt $2 ] 
then
    echo "  $1 is greater than $2"

elif [ $1 -eq $2 ]
then
    echo "  $1 is equal to $2"

else
    echo "  $2 is greater than $1"

fi

echo -e "\nTesting For Loops:"

# Print Bash is great
for i in $(seq $1)
do
    echo "  Bash is great"

done

echo -e "\nTesting Functions:"

# Square function
function square() {
    local x=$1
    result=$(( x * x ))
}

# Run square and print result
square $2
echo "  square $2: $result"

echo -e "\nTesting Arrays:"

# Create array with input
my_array=($1 $2)
echo "  array: ${my_array[@]}"
echo "  length of array[1]: ${#my_array[1]}"
echo "  length of array: ${#my_array[@]}"
echo 


