#/bin/bash

mount_point=$(dirname $BASH_SOURCE)
bin_path=$mount_point/bin
sbin_path=$mount_point/sbin
lib_path=$mount_point/lib
lib64_path=$mount_point/lib64

echo $mount_point
echo $bin_path
echo $sbin_path
echo $lib_path
echo $lib64_path

export PATH="$bin_path:$sbin_path"
export LD_LIBRARY_PATH="$lib_path:$lib64_path"
