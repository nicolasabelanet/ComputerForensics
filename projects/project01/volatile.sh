#!/bin/bash

echo "Date:"
date

echo -e "\n\nOperating system information:"
echo -e "\n\nResults for '/etc/os-release':"
cat /etc/os-release

echo -e "Results for 'lsb_release -a':"
lsb_release -a

echo -e "\n\nNetwork interfaces:"
echo -e "\n\nResults for 'ip addr':"
ip addr

echo -e "\n\nResults for 'ifconfig -a:"
ifconfig -a

echo -e "\n\nOpen ports:"
echo -e "\n\nResults for 'netstat -anp':"
netstat -anp

echo -e "\n\nOpen files:"
echo -e "Results for 'lsof -V':"
lsof -V

echo -e "\n\nProcesses: "
echo -e "\n\nResults for 'ps -ef':"
ps -ef

echo -e "\n\nRouting tables:"

echo -e "\n\nResults for 'netstat -rn':"
netstat -rn

echo -e "\n\nResults for 'route':"
route

echo -e "\n\nResults for 'ip route':"
ip route

echo -e "\n\nMounted file-systems:"
echo -e "\n\nResults for ' mount':"
mount

echo -e "\n\nResults for 'df':"
df

echo -e "\n\nCurrent users:"
echo -e "\n\nResults for 'w':"
w

echo -e "\n\nLast user to login:"
echo -e "\n\nResults for 'last'":
last

echo -e "\n\nFailed login attempts:"
echo -e "\n\nResults for 'lastb':"
lastb

echo -e "\n\nUser information:"
echo -e "\n\nResults for 'cat /etc/passwd':"
cat /etc/passwd

echo -e "\n\nResults for 'cat /etc/shadow':"
cat /etc/shadow

