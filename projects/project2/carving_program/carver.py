"""
Carve various file types from a binary file.

Author:
    Nicolas Abelanet
"""

# Imports
import re
import pathlib
import logging
import argparse
import humanfriendly

from typing import Protocol, Optional, NamedTuple

# Module variables
logger: logging.Logger = logging.getLogger(__name__)

# Type definitions
BytePattern = bytes

class FileSpec(NamedTuple):
    extension: str
    header: BytePattern
    footer: BytePattern
# FileSpec

class FileOffsets(NamedTuple):
    # Start of file offset
    start: int

    # End of file offset + 1 to match slicing syntax
    end: int
# File

class FileCarver(Protocol):

    def __init__(self, file_spec: FileSpec) -> None:
        ...

    def carve(self, file_dump: bytes) -> Optional[list[FileOffsets]]:
        ...

    @property
    def file_spec(self) -> FileSpec:
        ...

# FileCarver


# Class definitions
class EncapsulatedCarver():
    
    def __init__(self, file_spec: FileSpec) -> None:
        self._file_spec: FileSpec = file_spec
    # __init__

    @property
    def file_spec(self) -> FileSpec:
        return self._file_spec
    # extension

    def carve(self, file_dump: bytes) -> Optional[list[FileOffsets]]:
        """ 
        Carve files with a known header and footer
        from a file dump.
        """

        # Get the start offsets from all headers found in file dump
        headers: list[re.Match[bytes]] = list(re.finditer(self._file_spec.header, file_dump))
        logger.debug(f"  Found {len(headers)} header(s)")

        if len(headers) == 0:
            return

        footers: list[re.Match[bytes]] = list(re.finditer(self._file_spec.footer, file_dump))

        logger.debug(f"  Found {len(footers)} footers(s)")

        file_offsets: list[FileOffsets] = self._assemble_offsets(headers, footers)

        if len(file_offsets) == 0:
            return None

        return file_offsets
    # carve

    def _assemble_offsets(
                        self, 
                        headers: list[re.Match[bytes]], 
                        footers: list[re.Match[bytes]]) -> list[FileOffsets]:
        file_offsets: list[FileOffsets] = list()

        for header in headers:

            # Take the first occurance of a valid footer
            footer: Optional[re.Match[bytes]] = next(
                filter(
                    lambda f: f.start() > header.end(), 
                    footers
                ),
                None   
            )

            # Make sure a valid footer was found
            if footer is not None:
                logger.debug(f"  Valid footer at {hex(footer.start())}")
                file_offsets.append(FileOffsets(header.start(), footer.end()))
    
            else:
                logger.warning(f"  No valid footer was found for header at {hex(header.start())}")
        return file_offsets
    # _assemble_indices

# EncapsulatedCarver


class GreedyCarver():
    
    def __init__(self, file_spec: FileSpec) -> None:
        self._file_spec: FileSpec = file_spec
    # __init__

    def carve(self, file_dump: bytes) -> Optional[list[FileOffsets]]:
        """ 
        Carve a file with a known header until the 
        end of a file dump.
        """

        headers: list[re.Match[bytes]] = list(re.finditer(self._file_spec.header, file_dump))
        logger.debug(f"  Found {len(headers)} header(s)")

        if len(headers) == 0:
            return None

        if len(headers) > 1:
            logger.warning("This carver does not support more than a single header")
            return None
        
        header: re.Match[bytes] = headers[0]

        file_offsets: list[FileOffsets] = [
            FileOffsets(header.start(), len(file_dump))
        ]

        return file_offsets
    # carve

    @property
    def file_spec(self) -> FileSpec:
        return self._file_spec
    # file_type

# GIFCarver


def main() -> None:

    # Setup logging
    logger.setLevel(logging.DEBUG)

    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    logger.addHandler(ch)

    # Get arguments
    args_parser = argparse.ArgumentParser("Data Carver")
    args_parser.add_argument("filename", type=str)
    args = args_parser.parse_args()

    # Define specs
    gif_spec = FileSpec(
        extension="gif",
        header=b"\x47\x49\x46\x38\x39\x61",
        footer=b"\x00\x3B"
    )

    jpeg_spec = FileSpec (
        extension="jpeg",
        header=b"\xFF\xD8\xFF",
        footer=b"\xFF\xD9"
    )

    png_spec = FileSpec(
        extension="png",
        header=b"\x89\x50\x4E\x47\x0D\x0A\x1A\x0A",
        footer=b"\x00\x00\x00\x00\x49\x45\x4E\x44\xAE\x42\x60\x82",
    )

    # Create carvers
    gif = GreedyCarver(gif_spec)
    jpeg = EncapsulatedCarver(jpeg_spec)
    png = EncapsulatedCarver(png_spec)

    filename: str = args.filename

    logger.info(f"Carving {filename}")

    # Make carving passes with carvers
    with open (filename, "rb") as f:
        file_dump = f.read()

        carve_pass(file_dump, gif, filename)
        carve_pass(file_dump, png, filename)
        carve_pass(file_dump, jpeg, filename)
    
# main


def carve_pass(file_dump: bytes, carver: FileCarver, result_prefix: str) -> None:
    """
    Carve a given file type from a file dump.

    Args:
        file_dump: The file dump in bytes.
        carver: The file carver to use.
        result_prefix: The resulting carved files will 
            begin with this string.
    """

    logger.info(f"\nAttemping to carve {carver.file_spec.extension.upper()} files:")
    logger.debug(f"  Using {carver.__class__.__name__}")

    # Get offsets from file dump that represent a valid file
    file_offsets: Optional[list[FileOffsets]] = carver.carve(file_dump)

    if file_offsets is None:
        logger.info("  Found 0 files")
        return
    
    logger.info(f"  Found {len(file_offsets)} file(s)")

    # Pull file offsets from list and write to files
    for i, (start, end) in enumerate(file_offsets, 1):
        with open(f"{result_prefix}.{i}.{carver.file_spec.extension}", "wb") as f:
            logger.info(f"  {pathlib.Path(f.name).name}: ")

            # Slicing is [start, end) 
            # Start = offset of the start
            # End = offset of end + 1 to match slicing

            # Pull relevant data from file dump
            full_file: bytes = file_dump[start:end]

            # Adjust offset to be exactly the end
            logger.debug(f"    Adjusted file offsets [{hex(start)}, {hex(end-1)}]")

            logger.info(f"    Writing {humanfriendly.format_size(len(full_file))} -> {f.name}")    
            f.write(full_file)

# carve_pass


if __name__ == "__main__":
    main()
